<p align="left"> <img src="https://komarev.com/ghpvc/?username=tastekim&label=Profile%20views&color=0e75b6&style=flat" alt="tastekim" /> </p>

# ```Hello, world !```  
<a href="https://tastekim.notion.site/tastekim/tastekim_Devlog-fe856eb9ac6e416db3807c12fcab39c5" target="_blank"><img src="https://img.shields.io/badge/Notion-FFFFFF?style=flat&logo=Notion&logoColor=black"/></a>
<a href="https://www.instagram.com/tastekim_" target="_blank"><img src="https://img.shields.io/badge/INSTAGRAM-fab1a0?style=flat&logo=instagram&logoColor=FFFFFF"/></a>
<a href="" target="_blank"><img src="https://img.shields.io/badge/tastekim@kakao.com-fdcb6e?style=flat&logo=gmail&logoColor=FFFFFF"/></a>  

## 🎯같이 커피 한 잔 하고 싶은 개발자가 되고 싶습니다.

> 안녕하세요 ! 백엔드 개발자가 되기 위해 공부중인 **tastekim** 입니다.🤗  
  함께 일하고 싶은 스스로 생각하고 고민하는 개발자가 되도록 노력중입니다.
  모든 피드백은 환영합니다 !  
```javascript
class TasteKim {
    stacks = {
        Nodejs  : ["express"],
        DB      : ["MySQL", "mongoDB", "redis"],
        devtools: ["git", "AWS", "slack"]
    };

    interested = ["typescript", "Nest.js"];

    skill = async (err) => {
        let result = await Enjoy.resolve(err);
        result += Study.hard(whatever);
        return result;
    };
}

module.exports = TasteKim;
```
![snake gif](https://github.com/tastekim/tastekim/blob/output/github-contribution-grid-snake.svg)
## 🤝진행한 프로젝트s
- [We All Lie - 보드게임 '스파이 폴'을 모티브로 한 비대면 보드게임 서비스](https://github.com/tastekim/WeAllLie-BE)

